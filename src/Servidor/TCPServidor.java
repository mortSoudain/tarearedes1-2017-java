package Servidor;

import java.io.*; 
import java.net.*;


class TCPServidor extends Thread {
	
	int videoSeleccionado;
    BufferedReader inFromClient;
    PrintWriter outToClient;
    Socket connectionSocket;
	private static ServerSocket welcomeSocket;
	
	public static EmisorController emisor;
	
	  public TCPServidor(int puerto) throws IOException{
		  
	      welcomeSocket = new ServerSocket(puerto);
	      
	      while(true) { 
	            Socket connectionSocket = welcomeSocket.accept(); 
	           // crear hebra para nuevo cliente
	          TCPServidor  clienteNuevo = 
	        	new TCPServidor(connectionSocket);
	           clienteNuevo.start();
	      }
	     // si se usa una condicion para quebrar el ciclo while, se deben cerrar los sockets!
	     // connectionSocket.close(); 
	     // welcomeSocket.close(); 
		  
	  }

  
   public TCPServidor(Socket s){
	   videoSeleccionado=0;
        connectionSocket = s;
        try{
         inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream())); 
         outToClient =  new PrintWriter(connectionSocket.getOutputStream(), true); 
        }catch(IOException e){
	    System.out.println("Problema creacion socket");
        } 
   }
  
  public void run(){
		//El while es necesario por culpa del try
	  while(true){
        try{
        	interpreteDeComandos();
        }catch(IOException | InterruptedException e){
		    System.out.println("Problema lectura/escritura en socket");
		    return;
        }
	  }
      
  }
 
	private void interpreteDeComandos() throws IOException, InterruptedException {
				
	       outToClient.println("Bienvenido al sistema de mensajes TCP");
	        Thread.sleep(50);
	        
	        outToClient.println("Existen los siguientes videos en el servidor:");
	        Thread.sleep(50);
	        
	        outToClient.println("video_1");
	        Thread.sleep(50);
	        
	        outToClient.println("video_2");
	        Thread.sleep(50);
	        
	        outToClient.println("Escriba GET <video_#> para ver el video que desee");
	        Thread.sleep(50);
	        
	        
	        String comando = inFromClient.readLine();
	        
	        if(comando.contains("GET")){
	        	String nombreVideo=comando.substring(5, comando.length()-1);
	        	
	        	if(nombreVideo.equals("video_1")){
		            outToClient.println("OK 84");
		            videoSeleccionado=1;
	        	} else if(nombreVideo.equals("video_2")){
		            outToClient.println("OK 101");
		            videoSeleccionado=2;
	        	}
	        } else if(comando.contains("PORT")){
	        	int numeroPuerto=Integer.parseInt(comando.substring(5, comando.length()));
	        	
	        	if(videoSeleccionado==1){
		        	emisor=new EmisorController(84,"video_1","localhost", numeroPuerto);
	        	}else if (videoSeleccionado==2){
		        	emisor=new EmisorController(101,"video_2","localhost", numeroPuerto);
	        	}
	        }
	        
	        //this.connectionSocket.getInetAddress();
		
	}
	

} 
 
