package Servidor;
import java.io.ByteArrayOutputStream; 
import java.io.File;
import java.io.IOException;
import java.net.*; 
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

public class UDPEmisorImagenes implements Runnable{
		
	public int cantImagenes;
	public String ip = "localhost";
	public int puerto;
	public String nombreVideo;

	public static DatagramSocket socketEmisor;
	public static BufferedImage imagen;
	public static InetAddress IPReceptor;
	
	UDPEmisorImagenes(int cantImagenes,String nombreVideo, String ip, int puerto) throws SocketException, UnknownHostException {
		this.nombreVideo=nombreVideo;
		this.cantImagenes=cantImagenes;
		this.ip=ip;
		this.puerto = puerto;
		socketEmisor = new DatagramSocket();
		IPReceptor = InetAddress.getByName(ip);
		System.out.println("Enviando " +cantImagenes+ " imagenes a\n" +ip+ ":" +puerto+ "\n");
	}
		@Override
		public void run() {
				int i=0;
				while(i<cantImagenes){
					try {
						if(i==cantImagenes-1){
							imagen = ImageIO.read(new File("Videos/FIN.jpg"));
						} else {
							imagen = ImageIO.read(new File("Videos/"+nombreVideo+"/"+i+".jpg"));
						}
						ByteArrayOutputStream flujoBytes = new ByteArrayOutputStream();			//crea un flujo bytes
						ImageIO.write(imagen, "jpg", flujoBytes);
						byte[] sendData = flujoBytes.toByteArray();								//Transforma el flujo a arreglo
						socketEmisor.send(new DatagramPacket(sendData, sendData.length, IPReceptor, puerto));
						Thread.sleep(40);
					} catch (IOException | InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					//System.out.println("Enviada Imagen " +i);
					//System.out.println("de tama�o:" + sendData.length+ "\n");
					i++;
				}
				
				return;
			
		}
}