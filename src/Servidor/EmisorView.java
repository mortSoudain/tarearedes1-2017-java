package Servidor;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class EmisorView extends JFrame {
    
	EmisorView(){
		JLabel label = new JLabel("Enviando Imagenes...");
		JPanel panel = new JPanel();
        this.setSize(new Dimension(500,500));
        this.setTitle("Cliente");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        panel.add(label);
        this.add(panel);
        this.pack();
        this.setLocationRelativeTo(null);
        
        this.toFront();
        this.requestFocus();
        this.setVisible(true);
	}
}