package Servidor;

import java.net.SocketException;
import java.net.UnknownHostException;

public class EmisorController {
	
    public static Thread hiloUDPEmisor;
    public static EmisorView vista;
	
	EmisorController(int cantidadImagenes, String nombreVideo, String IP, int PUERTO) throws SocketException, UnknownHostException{
	    vista = new EmisorView();
		hiloUDPEmisor = new Thread(new UDPEmisorImagenes(cantidadImagenes,nombreVideo,IP,PUERTO));
		hiloUDPEmisor.start();
	}
	
}