package Cliente;
import java.io.ByteArrayInputStream; 
import java.io.File;
import java.io.IOException;
import java.net.*; 
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

public class UDPReceptorImagenes implements Runnable {
	
	
	public String nombreVideo;
	public static int cantImagenes;
	public static int maxSize; //tamaNo maximo de cada foto
	public static int puerto;
	public static DatagramSocket socketReceptor;
	public static BufferedImage imagen;
	public static DatagramPacket paqueteReceptor;
	
	UDPReceptorImagenes(int cantImagenes,String nombreVideo, int maxSize, int puerto) throws Exception{
		this.nombreVideo=nombreVideo;
		UDPReceptorImagenes.cantImagenes = cantImagenes;
		UDPReceptorImagenes.maxSize = maxSize;
		UDPReceptorImagenes.puerto = puerto;
        socketReceptor = new DatagramSocket(puerto);
        System.out.println("Esperando " +cantImagenes+ " imagenes en puerto " +puerto+"\n");
	}
	@Override
	public void run () {
		int i=0;
		while(i<cantImagenes){
            //System.out.println("Esperando Imagen " +i);
            paqueteReceptor = new DatagramPacket(new byte[maxSize], maxSize);
            try {
				socketReceptor.receive(paqueteReceptor);
				imagen = ImageIO.read(new ByteArrayInputStream(paqueteReceptor.getData()));
				File outputFile = new File("VideosRecibidos/"+this.nombreVideo+"/"+i+".jpg");
				outputFile.getParentFile().mkdirs();
				ImageIO.write(imagen,"jpg",outputFile);
				
				Thread.sleep(40);
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
            i++;
		}
		return;
	}
}