package Cliente;

import java.io.*; 
import java.net.*;

class TCPCliente {
		
	Socket clientSocket;
	PrintWriter outToServer;
	
    static String sentence;
    String echoSentence;
    
    HiloLecturaServer hiloLectura;
    
    public TCPCliente(String ip, int puerto) throws Exception{
        //Conexion al server
        clientSocket = new Socket(ip, puerto);
        
        //Escritura y lectura al socket del server
        outToServer = new PrintWriter(clientSocket.getOutputStream(), true); 
        hiloLectura = new HiloLecturaServer(clientSocket);
    			
    	//Ciclo de escritura en server.    	
        do {
        	//Input desde teclado
            BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));                            
            sentence = inFromUser.readLine();
            
            if(sentence.contains("GET")){
            	hiloLectura.nombreVideo=sentence.substring(5, sentence.length()-1);
            }
            
            //Env�o hacia el server del input
            outToServer.println(sentence);
            
        } while(!sentence.equals("0"));
        
        clientSocket.close(); 
    }
}