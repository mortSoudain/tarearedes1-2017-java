package Cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Random;

public class HiloLecturaServer extends Thread {
	
	public String nombreVideo;
	
	ReceptorController receptor;
	
	BufferedReader inFromServer;
	PrintWriter outToServer;
	Socket clientSocket;
	
	//CONSTRUCTOR
	
	public HiloLecturaServer(Socket clientSocket) throws IOException{
        outToServer = new PrintWriter(clientSocket.getOutputStream(), true);
		inFromServer=new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		this.clientSocket=clientSocket;
		this.start();
	}
	
	public void run(){
		//El while es necesario por culpa del try
		while(true){
		try {
			String echoSentence = inFromServer.readLine();
			
			if(echoSentence.contains("OK")){
	        	int cantidadImagenes=Integer.parseInt(echoSentence.substring(3, echoSentence.length()));
				int puertoAleatorio=puertoAleatorio();
		    	receptor= new ReceptorController(cantidadImagenes,puertoAleatorio,nombreVideo);
	            outToServer.println("PORT "+puertoAleatorio); 
			}
			
			System.out.println("SERVIDOR: " + echoSentence);	


		} catch (Exception e) {
			e.printStackTrace();
		}
		}
	}
	
	int puertoAleatorio(){
		Random random = new Random();
		int numero_aletorio = random.nextInt((8000-1024)+1) + 1024;
		return numero_aletorio;
	}

}
