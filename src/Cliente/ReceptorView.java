package Cliente;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame;

@SuppressWarnings("serial")
public class ReceptorView extends JFrame implements Runnable{

	public static String nombreVideo;	
	public static int cantImagenes;	
	public static Canvas canvas;
    public static BufferedImage imagen;
    
	ReceptorView(int cantImagenes, String nombreVideo){
		ReceptorView.cantImagenes=cantImagenes;
		ReceptorView.nombreVideo=nombreVideo;
		canvas = new Canvas();
        canvas.setPreferredSize(new Dimension(1182,664));
        this.setSize(new Dimension(1182,664));
        this.setTitle("Videos Sofanor");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.add(canvas);
        this.pack();
        this.setLocationRelativeTo(null);
        
        this.toFront();
        this.requestFocus();
        this.setVisible(true);
	}
	
    public static void mostrar(){     
        BufferStrategy estrategia = canvas.getBufferStrategy();
        if(estrategia == null){
            canvas.createBufferStrategy(3);
            return;
        }
        Graphics g = estrategia.getDrawGraphics(); 
        g.drawImage(imagen, 0, 0, 1182, 664, null);
        g.dispose();
        estrategia.show();
    }

	@Override
	public void run() {
		int i=0;
		while(true){
			try {
				imagen=ImageIO.read(new File("VideosRecibidos/"+ReceptorView.nombreVideo+"/"+i+".jpg"));
				mostrar();
				Thread.sleep(40);
			} catch (IOException | InterruptedException ex) {
                ex.toString();
            }
			i++;
			if(i==cantImagenes) i=0;
		}
			
	}
}