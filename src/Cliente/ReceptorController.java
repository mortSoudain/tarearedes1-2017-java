package Cliente;

public class ReceptorController {
	
	public static final int MAX_SIZE=100000;
    public static Thread hiloVista;
    public static Thread hiloUDPReceptor;
	
	public ReceptorController(int cantidadImagenes, int puerto, String nombreVideo) throws Exception{
		hiloUDPReceptor = new Thread(new UDPReceptorImagenes(cantidadImagenes,nombreVideo,MAX_SIZE,puerto));
		hiloVista = new Thread(new ReceptorView(cantidadImagenes,nombreVideo));
		
		hiloUDPReceptor.start();
		hiloVista.start();
	}
}